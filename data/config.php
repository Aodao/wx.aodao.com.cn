<?php
defined('IN_IA') or exit('Access Denied');

$config = array();


$config['db']['master']['host'] = '127.0.0.1';
$config['db']['master']['username'] = 'root';
$config['db']['master']['password'] = 'root';
$config['db']['master']['port'] = '3306';
$config['db']['master']['database'] = 'wxy';
$config['db']['master']['charset'] = 'utf8';
$config['db']['master']['pconnect'] = 0;
$config['db']['master']['tablepre'] = 'ims_';



$config['db']['common']['slave_except_table'] = array('core_sessions');

// --------------------------  CONFIG COOKIE  --------------------------- //
$config['cookie']['pre'] = 'aa13_';
$config['cookie']['domain'] = '';
$config['cookie']['path'] = '/';

// --------------------------  CONFIG SETTING  --------------------------- //
$config['setting']['charset'] = 'utf-8';
$config['setting']['cache'] = 'mysql';
$config['setting']['timezone'] = 'Asia/Shanghai';
$config['setting']['memory_limit'] = '256M';
$config['setting']['filemode'] = 0644;
$config['setting']['authkey'] = '22c1fba6';
$config['setting']['founder'] = '1';
$config['setting']['development'] = 1;
$config['setting']['referrer'] = 0;
$config['setting']['https'] = 0; //0为关闭https,1为开启 端口443 配置共存模式不能开启1
// --------------------------  CONFIG UPLOAD  --------------------------- //
$config['upload']['image']['extentions'] = array('gif', 'jpg', 'jpeg', 'png');
$config['upload']['image']['limit'] = 5000;
$config['upload']['attachdir'] = 'attachment';
$config['upload']['audio']['extentions'] = array('mp3');
$config['upload']['audio']['limit'] = 5000;

// --------------------------  CONFIG MEMCACHE  --------------------------- //
$config['setting']['cache'] = 'redis';
$config['setting']['redis']['server'] = '127.0.0.1';
$config['setting']['redis']['port'] = '6379';
$config['setting']['redis']['pconnect'] = '1';
$config['setting']['redis']['timeout'] = '30';
$config['setting']['redis']['session'] = '1';

$config['setting']['cache'] = 'memcache';
$config['setting']['memcache']['server'] = '127.0.0.1';
$config['setting']['memcache']['port'] = '11211';
$config['setting']['memcache']['pconnect'] = '1';
$config['setting']['memcache']['timeout'] = '30';
$config['setting']['memcache']['session'] = '1';

// --------------------------  CONFIG PROXY  --------------------------- //
$config['setting']['proxy']['host'] = '';
$config['setting']['proxy']['auth'] = '';

//++--------------- zio_domain 域名绑定配置请不要手工修改 ---------------//

//$config['setting']['domain']['host']='127.0.0.1:1919';
//$config['setting']['domain']['protect_app']='0';
//$config['setting']['domain']['protect_web']='0';
//$config['setting']['domain']['tip']='域名授权异常,请联系技术客服重新授权!';
//if(file_exists(IA_ROOT . "/addons/zio_domain/domain.php")){
//    include IA_ROOT . "/addons/zio_domain/domain.php";}

